# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

### Setup de deux machines Rocky Linux configurée de façon basique.

#### Un accès internet (via la carte NAT)

On peux montrer que la carte NAT est présente grâce à la commande `ip a`:
```
[mattox@node1 ~]$ ip a
(...)
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:68:1b:12 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86262sec preferred_lft 86262sec
    inet6 fe80::a00:27ff:fe68:1b12/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

On peut voir que on a accès a internet en essayant de ping de DNS de google et son adresse `8.8.8.8` :

```
[mattox@node1 .ssh]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=100 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=23.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=392 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=113 ms
--- 8.8.8.8 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5008ms
rtt min/avg/max/mdev = 23.046/120.169/392.356/126.400 ms
----------------------------------------------------------------
[mattox@node1 ~]$ ping google.com
PING google.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=113 time=18.3 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=113 time=19.0 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=113 time=18.5 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 18.281/18.598/19.004/0.321 ms
```

#### Un accès à un réseau local

Nous commencons par definir une adresse IP statique pour les deux cartes Host-Only.

Pour cela, il faut aller dans le dossier `/etc/sysconfig/network-scripts`, puis modifier le fichier `ifcfg-enp0s8` comme ceci : 

```
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=f6016371-9ca6-42ad-b5ad-d106353a872a
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.11
NETMASK=255.255.255.0
DNS1=1.1.1.1
```

Où on change `BOOTPROTO=dhcp` en `BOOTPROTO=static`, `ONBOOT=no` en `ONBOOT=yes`, on ajoute `IPADDR=10.101.1.11` et `IPADDR=10.101.1.12` pour configurer l'adresse IP et `NETMASK=255.255.255.0` pour configurer le masque.

Ensuite on redemarre l'interface en faisant `sudo nmcli con reload` et `sudo nmcli con up enp0s8`

```
[mattox@bastion-ovh1fr network-scripts]$ sudo nmcli con reload
[mattox@bastion-ovh1fr network-scripts]$ sudo nmcli con up enp0s8
Connection succesfully activated (D-Bus active path: /org/freedesktop/Networkmanager/ActiveConnection/3)
```


On modifie ceci pour les deux machines virtuelles et ils obtiennent les adresses IP `10.101.1.11` et `10.101.1.12`

```
[mattox@node1 /]$ ip a
(...)
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:bd:af:55 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.10.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:febd:af55/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Comme on peut le voir ici, en faisant un `ip a`, `enp0s8` a pour IP `10.101.1.11`

Les deux machines peuvent se ping, on se place sur la premiere VM ayant pour IP `10.101.1.11` et on essaye de ping vers `10.101.1.12`
```
[mattox@node1 /]$ ping 10.101.1.12
PING 10.101.1.12 (10.10.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.58 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.904 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=1.00 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=0.991 ms
^C
--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.904/1.120/1.582/0.269 ms
```

#### Les machines doivent avoir un nom

Pour modifier le nom temporairement, on utilise la commande `sudo hostname <HOSTNAME>`

```
[mattox@node1 ~]$ sudo hostname node1.tp2.b2
[sudo] password for mattox:
[mattox@node1 ~]
```

Pour definir le nom de la machine permanent, il suffit de taper `echo 'node1.tp1.b2' | sudo tee /etc/hostname`, et notre première machine virtuelle se nommera `node1.tp1.b2`, On refait la même manipulation pour la deuxieme machine avec le nom `node2.tp1.b2`
```
[mattox@node1 /]$ echo 'node1.tp2.b2' | sudo tee /etc/hostname
node1.tp2.b2
[mattox@node1 /]$ hostname
node1.tp1.b2
```
On peut voir en tapant la commande `hostname` que le changement a bien été pris en compte

#### Utiliser `1.1.1.1` comme serveur DNS :

Pour utiliser `1.1.1.1` comme DNS, on peux modifier le fichier `/etc/resolv.conf` des deux machines comme ceci :
```
[mattox@node1 ~]$ sudo nano /etc/resolv.conf

# Generated by NetworkManager
search lan tp1.b2
nameserver 1.1.1.1
```
ou alors on peut ajouter la ligne `DNS1=1.1.1.1` dans le fichier de configuration d'interface `/etc/sysconfig/network-scripts/ifcfg-enp0s8`

```
[mattox@node1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8

TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=f6016371-9ca6-42ad-b5ad-d106353a872a
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.10.1.11
NETMASK=255.255.255.0
DNS1=1.1.1.1
```

* Vérifier avec le bon fonctionnement avec la commande dig : 

On demande une résolution du nom `ynov.com` avec la commande `dig ynov.com`

```
[mattox@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 22095
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 235 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 18 14:13:02 CEST 2021
;; MSG SIZE  rcvd: 53
```

On peut reperer la ligne qui corresponds à la réponse, avec l'IP qui corresponds au nom de domaine `ynov.com` qui est donc `92.243.16.143`

```
;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143
```

Et on peut reperer l'adresse IP du serveur qui nous as répondu qui est donc `1.1.1.1`, les changements ont bien été pris en compte.

```
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

#### Les machines doivent pouvoir se joindre par leurs noms respectifs

On modifie le fichier `/etc/hosts` pour ajouter les noms de domaines des differentes machines, donc par exemple sur `node1` on va faire correspondre l'adresse IP de `node2` avec son nom de domaine : 

```
[mattox@node1 /]$ sudo nano /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12 node2 node2.tp1.b2
```

Donc ici, les noms de domaines `node2` et `node2.tp1.b2` correspondent à l'adresse IP `10.101.1.12` (celle de `node2`)

On peut voir qu'on arrive à ping `node2.tp1.b2` : 

```
[mattox@node1 /]$ ping node2.tp1.b2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.939 ms
64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.962 ms
64 bytes from node2 (10.101.1.12): icmp_seq=3 ttl=64 time=1.02 ms
^C
--- node2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 0.939/0.974/1.022/0.043 ms
```

On fais la même démarche sur la deuxieme machine Linux, 

```
[mattox@node2 ~]$ sudo nano /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.11  node1 node1.tp1.b2
--------------------------------------------
[mattox@node2 ~]$ ping node1.tp1.b2
PING node1 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.583 ms
64 bytes from node1 (10.101.1.11): icmp_seq=2 ttl=64 time=0.559 ms
64 bytes from node1 (10.101.1.11): icmp_seq=3 ttl=64 time=0.530 ms
^C
--- node1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2068ms
rtt min/avg/max/mdev = 0.530/0.557/0.583/0.029 ms
```

#### Pare-feu

On peut taper la commande `sudo firewall-cmd --list-all` pour vérifier le firewall

```
[mattox@node1 /]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

* On crée un utilisateur `matt` où on place son repertoire home avec `-m` où /home est le repertoire par défaut et le `shell` par défaut de l'utilisateur dans `/bin/bash` avec `-s`

```
[mattox@node1 ~]$ sudo useradd matt -m -s /bin/bash
[sudo] password for mattox:
[mattox@node1 ~]$
-------------------------------------------------------------
[mattox@node1 ~]$ cat /etc/passwd
(...)
matt:x:1001:1001::/home/matt:/bin/bash
```

* On crée ensuite un groupe `admins` grace à la commande `groupadd <groupe>`

```
[mattox@node1 ~]$ sudo groupadd admins
[sudo] password for mattox:
[mattox@node1 ~]$
-------------------------------------------------------------
[mattox@node1 ~]$ cat /etc/group
(...)
admins:x:1002:
```

* On utilise la commande `sudo visudo` pour modifier le fichier `/etc/sudoers` en ajoutant la condition pour que le groupe `admins` aie tous les droits

```
[mattox@node1 ~]$ sudo visudo

## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
%admins ALL=(ALL)       ALL
```

* On ajoute ensuite l'utilisateur `matt` au groupe `admins` avec la commande `usermod -aG admins <user>` 

```
[mattox@node1 ~]$ sudo usermod -aG admins matt
[sudo] password for mattox:
[mattox@node1 ~]$`
```

On peut voir que les modifications ont été effectués avec la commande `groups <user>`
```
[mattox@node1 ~]$ groups matt
matt : matt admins`
```
l'utilisateur `matt` est bien dans le groupe `admins`

### 2. SSH






#### Vous n'utilisez QUE `  ` pour administrer les machines

* ssh-copy-id ip

On lance un terminal sur mon PC (Windows), on se place dans le repertoire `C:\Users\matte\.ssh` et on tape la commande `ssh-keygen -t rsa -b 4096` pour creer une paire de clé

```
C:\Users\matte\.ssh>ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\matte/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\matte/.ssh/id_rsa.
Your public key has been saved in C:\Users\matte/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:JLyu0h3t6eZz3mH16QvvxVam1rQGm3oxddxt86ecqDE matteo@LAPTOP-UD8C09I0
The key's randomart image is:
+---[RSA 4096]----+
|                 |
|     .           |
|      o .      .o|
|       +       o*|
|      ..S    .o.B|
|     .. .    ++B*|
|   . ..o .E o*=*B|
|  . ... = .=o+O..|
|   ..  +o+ooo .=.|
+----[SHA256]-----+
```

Pour déposer la clé sur notre machine Linux, on utilise la commande `scp`

```
C:\Users\matte\.ssh>scp C:\Users\matte\.ssh\id_rsa.pub matt@10.101.1.11:/home/matt/.ssh/authorized_key
matt@10.101.1.11's password:
id_rsa.pub                                                                         100%  577   192.4KB/s   00:00
```

Pour se connecter en ssh, on utilise la commande `ssh <user@host> -i <chemin vers clé privée>` pour lui indiquer le chemin de la clé privée

```
C:\Users\matte\.ssh>ssh mattox@10.101.1.11 -i C:/Users/matte/.ssh/id_rsa
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Sep 23 10:12:58 2021
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Sep 23 10:12:58 2021
[mattox@node1 ~]$`
```

## II. Partitionnement

### 2. Partitionnement

Tout d'abord on repere les deux disques que l'on vient de creer avec la commande `lsblk`, ici les deux disques sont `sdb` et `sdc`

```
[mattox@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```
Ensuite on ajoute les disques en tant que PV (Physical Volume) 
```
[mattox@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for mattox:
  Physical volume "/dev/sdb" successfully created.
[mattox@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
```
On peut voir qu'ils ont bien été créer avec la commande `sudo pvs`
```
[mattox@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   3.00g 3.00g
  /dev/sdc      lvm2 ---   3.00g 3.00g
```
On crée ensuite un VG (Volume Group) qu'on nomme `vgtp1`

```
[mattox@node1 ~]$ sudo vgcreate vgtp1 /dev/sdb
[sudo] password for mattox:
  Volume group "vgtp1" successfully created
```
On ajoute `/deb/sdc` au Volume Group
```
[mattox@node1 ~]$ sudo vgextend vgtp1 /dev/sdc
  Volume group "vgtp1" successfully extended
```
On peux voir que les PV sont bien présent dans le VG, avec `Cur PV` qui est à 2 et `Metadata Areaas` qui est à 2 lui aussi. On peut aussi voir que la taille total de notre VG est de 6 Go, ce qui corresponds à la taille des deux PV
```
[mattox@node1 ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               vgtp1
  System ID
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               5.99 GiB
  PE Size               4.00 MiB
  Total PE              1534
  Alloc PE / Size       0 / 0
  Free  PE / Size       1534 / 5.99 GiB
  VG UUID               gMotKf-9mkd-31xz-123H-3wyH-xxsH-N8RbUv
```

* Créons maintenant 3 logical volumes de 1 Go chacun

On crée 3 Logical Volume de 1Go chacun grâce à la commande `sudo lvcreate -L <SIZE> <VG_NAME> -n <LV_NAME>` où `-L` précise la taille

```
[mattox@node1 ~]$ sudo lvcreate -L 1G vgtp1 -n LV_TP1_1
[sudo] password for mattox:
  Logical volume "LV_TP1_1" created.
[mattox@node1 ~]$ sudo lvcreate -L 1G vgtp1 -n LV_TP1_2
  Logical volume "LV_TP1_2" created.
[mattox@node1 ~]$ sudo lvcreate -L 1G vgtp1 -n LV_TP1_3
  Logical volume "LV_TP1_3" created.
```

On peut verifier qu'ils sont présentes avec la commande `sudo lvs` 

```
[mattox@node1 ~]$ sudo lvs
  LV       VG    Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root     rl    -wi-ao----  <6.20g
  swap     rl    -wi-ao---- 820.00m
  LV_TP1_1 vgtp1 -wi-a-----   1.00g
  LV_TP1_2 vgtp1 -wi-a-----   1.00g
  LV_TP1_3 vgtp1 -wi-a-----   1.00g
```

* Formatons ces partitions

On peux monter les partitions des 3 LVs grâce à la commande `mkfs -t <FS> <PARTITION>` où on formate en `ext4`

```
[mattox@node1 ~]$ sudo mkfs -t ext4 /dev/vgtp1/LV_TP1_1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 2ff8d7e1-6bd6-4dfa-9111-f6dceb8ba57a
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```
Et on fais pareil pour les 2 autres LVs

* Montons ces partitions

On créer d'abord un dossier `/mnt/part1` pour notre point de montage

```
[mattox@node1 ~]$ sudo mkdir /mnt/part1
[mattox@node1 ~]$
```
Puis on utilise la commande `mount <PARTITION> <MOUNT_POINT>` pour monter les partitions sur le point de montage.
```
[mattox@node1 ~]$ sudo mount /dev/vgtp1/LV_TP1_1 /mnt/part1
[mattox@node1 ~]$
```
Et on fais la meme manipulation pour les deux autres partitions.

On peux vérifier avec la commande `df -h` que les partitions soit bien effectives

```
[mattox@node1 ~]$ df -h
Filesystem                  Size  Used Avail Use% Mounted on
devtmpfs                    891M     0  891M   0% /dev
(...)
tmpfs                       182M     0  182M   0% /run/user/1000
/dev/mapper/vgtp1-LV_TP1_1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/vgtp1-LV_TP1_2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/vgtp1-LV_TP1_3  976M  2.6M  907M   1% /mnt/part3
```

* Faisons en sorte que les partitions soit montée automatiquement au démarrage du système.

Il faut modifier le fichier `/etc/fstab` et lui ajouter nos nouvelles partitions.

```
[mattox@node1 ~]$ sudo nano /etc/fstab
#
# /etc/fstab
# Created by anaconda on Wed Sep 15 13:25:15 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=6ab724a4-fbb3-4ca1-9a7f-97ce638972cb /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/vgtp1/LV_TP1_1 /mnt/part1 ext4 defaults 0 0
/dev/vgtp1/LV_TP1_2 /mnt/part2 ext4 defaults 0 0
/dev/vgtp1/LV_TP1_3 /mnt/part3 ext4 defaults 0 0
```
Pour vérifier si nos modifications sont pris en compte, on démonte la partition, pour la remonter avec les informations de `/etc/fstab`

```
[mattox@node1 ~]$ sudo umount /mnt/part1
[mattox@node1 ~]$ sudo umount /mnt/part2
[mattox@node1 ~]$ sudo umount /mnt/part3
-----------------------------------------------------
[mattox@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
mount: /mnt/part2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part2               : successfully mounted
mount: /mnt/part3 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part3               : successfully mounted
```
On démonte donc les 3 partions avce la commande `umount <MOUNT_POINT>` et on les rémonte avec les infos de `/etc/fstab` avec la commande `mount -av` et on peut voir que les partions ont été montés correctement

## III. Gestion de services

### 1. Interaction avec un service existant

* Pour le pare-feu `firewalld` on peut verifier si il est activé avec la commande `systemctl is-active <command>`

```
[mattox@node1 ~]$ sudo systemctl is-active firewalld
active
```
L'unité est donc activé

On peut aussi savoir si l'unité est activé au démarrage grâce à la commande `systemctl is-enabled <command>` 

```
[mattox@node1 ~]$ sudo systemctl is-enabled firewalld
enabled
```
L'unité est activé au démarrage

### 2. Création de service

Tout d'abord nous ouvrons le port 8888 

```
[mattox@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp
[sudo] password for mattox:
success
```

* On crée un fichier `web.service` dans le répertoire `/etc/systemd/system`

```
[mattox@node1 ~]$ cd /etc/systemd/system
[mattox@node1 system]$ sudo touch web.service
```

On dépose ces parametres dans le fichier `web.service`

```
[mattox@node1 system]$ cat web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

Pour démarrer le service, il faut installer python3 avec la commande `dnf install python3`

Ensuite, on demande à `systemd `de relire les fichiers de configuration

```
[mattox@node1 system]$ sudo systemctl daemon-reload
```

Il suffit de démarrer le serveur web
```
[mattox@node1 system]$ sudo systemctl start web
```

On peut regarder son status et voir qu'il est activé

```
[mattox@node1 system]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-09-23 12:33:31 CEST; 3s ago
 Main PID: 4460 (python3)
    Tasks: 1 (limit: 11397)
   Memory: 10.3M
   CGroup: /system.slice/web.service
           └─4460 /bin/python3 -m http.server 8888

Sep 23 12:33:31 node1.tp2.b2 systemd[1]: Started Very simple web service.
```

On vérifie qu'on peux accéder au service en faisant une requête curl sur le port 8888

```
[mattox@node1 system]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

### B. Modification de l'unité

* On crée un utilisateur `web`

```
[mattox@node1 system]$ sudo useradd web
[sudo] password for mattox:
[mattox@node1 system]$
---------------------------------------------------------------
[mattox@node1 system]$ cat /etc/passwd
(...)
web:x:1002:1003::/home/web:/bin/bash
```

* On ajoute web au groupe `admins` pour lui donner les droits

```
[mattox@node1 /]$ sudo usermod -aG admins web
```

* On modifie le fichier `/etc/systemd/system/web.service` pour ajouter l'utilisateur `web` et ajouter le `WorkingDirectory` pour lancer le serveur dans un repertoire spécifique

```
[mattox@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/TP1_web

[Install]
WantedBy=multi-user.target
```

* On crée ensuite un dossier `/srv/TP1_web` en tant que `web` et on place un fichier `test.txt` dans ce dossier

```
[web@node1 srv]$ sudo mkdir TP1_web
[web@node1 srv]$ cd TP1_web/
[web@node1 TP1_web]$ sudo touch test.txt
```

* On vérifie ensuite le fonctionnement des modifications avec la commande `curl`

```
[mattox@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="test.txt">test.txt</a></li>
</ul>
<hr>
</body>
</html>
```
On voit que tout marche parfaitement