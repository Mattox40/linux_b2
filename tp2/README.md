# TP2 pt. 1 : Gestion de service

# I. Un premier serveur web

## 1. Installation

#### 🌞 Installer le serveur Apache

**paquet `httpd`**

```bash
[mattox@web ~]$ sudo dnf install httpd
(...)
Installed:
  apr-1.6.3-11.el8.1.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-39.module+el8.4.0+571+fd70afb1.x86_64
  httpd-filesystem-2.4.37-39.module+el8.4.0+571+fd70afb1.noarch
  httpd-tools-2.4.37-39.module+el8.4.0+571+fd70afb1.x86_64
  mod_http2-1.15.7-3.module+el8.4.0+553+7a69454b.x86_64
  rocky-logos-httpd-84.5-8.el8.noarch

Complete!
```
On supprime tout les commentaires dans le fichier `/etc/httpd/conf/httpd.conf` à l'aide de `vim`

#### 🌞 Démarrer le service Apache
```bash
# On démarre le service httpd
[mattox@web ~]$ sudo systemctl start httpd

# On configure le service httpd pour qu'il démarre automatiquement
[mattox@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

# On repere le port où tourne Apache
[mattox@web ~]$ sudo ss -alnpt
State          Recv-Q         Send-Q                  Local Address:Port                   Peer Address:Port         Process
LISTEN         0              128                           0.0.0.0:22                          0.0.0.0:*             users:(("sshd",pid=833,fd=5))
LISTEN         0              128                                 *:80                                *:*             users:(("httpd",pid=24392,fd=4),("httpd",pid=24391,fd=4),("httpd",pid=24390,fd=4),("httpd",pid=24388,fd=4))
LISTEN         0              128                              [::]:22                             [::]:*             users:(("sshd",pid=833,fd=7))

# C'est donc le port 80, on ouvre donc ce port
[mattox@web ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
success
[mattox@web ~]$ sudo firewall-cmd --reload
success
```
#### 🌞 TEST

```bash
# On verifie que le service est démarré
[mattox@web ~]$ sudo systemctl is-active httpd
active

# On verifie que le service est démarrer automatiquement
[mattox@web ~]$ sudo systemctl is-enabled httpd
enabled

# On verifie que nous joignons notre serveur web localement
[mattox@web ~]$ curl localhost
(...)
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software it working
            correctly.</p>
      </div>
(...)
      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
(...)
```
On verifie que nous ayons acces depuis notre PC, on tape dans le navigateur `10.102.1.11:80` et on tombe sur la page d'acceuil de Apache

## 2. Avancer vers la maîtrise du service

#### 🌞 Le service Apache...

**donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume**

La commande est `sudo systemctl enable httpd`

**prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume**

La commande qui peut prouver que le service est bien paramétré est `sudo systemctl is-enabled httpd`

**affichez le contenu du fichier httpd.service qui contient la définition du service Apache**

```bash
# On affiche le contenu du fichier httpd.service
[mattox@web ~]$ sudo systemctl cat httpd
(...)

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
#### 🌞 Déterminer sous quel utilisateur tourne le processus Apache

```bash
# Cherchons l'utilisateur qui est utilisé dans le fichier de conf
[mattox@web ~]$ cat /etc/httpd/conf/httpd.conf | grep User
User apache
(...)

# Apache tourne bien sous l'utilisateur "apache"
[mattox@web ~]$ ps -ef | grep apache
apache     24389   24388  0 16:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24390   24388  0 16:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24391   24388  0 16:49 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache     24392   24388  0 16:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
(...)

# On peux voir que tous les dossiers sont lisibles et éxécutables pour 
# tous, et donc bien par notre utilisateur apache
[mattox@web ~]$ ls -al /var/www
total 4
drwxr-xr-x.  4 root root   33 Sep 29 16:34 .
drwxr-xr-x. 22 root root 4096 Sep 29 16:34 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
```

#### 🌞 Changer l'utilisateur utilisé par Apache

```bash
# On regarde la configuration de l'appareil 
[mattox@web ~]$ cat /etc/passwd | grep apache
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
# On repere que le repertoire personel de apache est "/usr/share/httpd"
# et que le shell par défaut est "/sbin/nologin

# On peut donc créer notre utilisateur "adminapache" avec les informations donnés
[mattox@web /]$ sudo useradd adminapache -m -d /usr/share/httpd -s /sbin/nologin
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.

# On vérifie que l'utilisateur a bien été créée
[mattox@web /]$ cat /etc/passwd | grep adminapache
adminapache:x:1001:1001::/usr/share/httpd:/sbin/nologin

# On modifie donc le fichier de configuration d'Apache pour qu'il utilise "adminapache"
[mattox@web /]$ sudo nano /etc/httpd/conf/httpd.conf
[mattox@web /]$ cat /etc/httpd/conf/httpd.conf | grep User
User adminapache

# On redemarre Apache
[mattox@web /]$ sudo systemctl restart httpd

# On verifie que le changement a bien pris effet
[mattox@web httpd]$ ps -ef | grep admin
adminap+    1769    1767  0 00:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
adminap+    1770    1767  0 00:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
adminap+    1771    1767  0 00:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
adminap+    1772    1767  0 00:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
# Le changement a bien pris effet 
# (le nom adminapache est trop long donc il a été rogné)
```

#### 🌞 Faites en sorte que Apache tourne sur un autre port

```bash
# On modifie la configuration d'Apache pour ecouter sur un autre port
[mattox@web httpd]$ sudo nano /etc/httpd/conf/httpd.conf

# Ici j'ai choisi d'écouter sur le port 100
[mattox@web httpd]$ cat /etc/httpd/conf/httpd.conf | grep Listen
Listen 100

# On ouvre le port 100, on ferme le port 80 et on rafraichit le firewall
[mattox@web httpd]$ sudo firewall-cmd --zone=public --add-port=100/tcp --permanent
success
[mattox@web httpd]$ sudo firewall-cmd --zone=public --remove-port=80/tcp --permanent
success
[mattox@web httpd]$ sudo firewall-cmd --reload
success

# On redemarre Apache
[mattox@web httpd]$ sudo systemctl restart httpd

# Apache tourne bien sur le nouveau port choisi
[mattox@web httpd]$ sudo ss -alnpt | grep 100
LISTEN 0      128                *:100             *:*    users:(("httpd",pid=2035,fd=4),("httpd",pid=2034,fd=4),("httpd",pid=2033,fd=4),("httpd",pid=2030,fd=4))

# On peux joindre Apache sur le nouveau port avec curl
[mattox@web httpd]$ curl localhost:100
(...)
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software it working
            correctly.</p>
      </div>
(...)
<footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>
(...)
```
On verifie que nous avons acces depuis notre PC, on tape dans le navigateur `10.102.1.11:100` et on tombe sur la page d'acceuil de Apache

On exporte le fichier `/etc/httpd/conf/httpd.conf` qu'on renomme `httpd1.conf` pour eviter les doublures

=> [httpd1.conf](./ressources/httpd1.conf)

# II. Une stack web plus avancée

## 2. Setup

### A. Serveur Web et NextCloud

#### 🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

```bash
# On commencer par reinitialiser notre serveur Apache
[mattox@web ~]$ sudo vi /etc/httpd/conf/httpd.conf
[mattox@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
Listen 80

User apache
Group apache
(...)
[mattox@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[mattox@web ~]$ sudo firewall-cmd --remove-port=100/tcp --permanent

# On commence par télécharger l'EPEL
[mattox@web /]$ sudo dnf install epel-release
(...)
Installed:
  epel-release-8-13.el8.noarch

Complete!

# Ensuite on lance une mise à jour pour s'assurer que nous sommes à la toute dernière version d'epel
[mattox@web /]$ sudo dnf update

# On installe le dépot Rémi
[mattox@web /]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
(...)
Installed:
  remi-release-8.4-1.el8.remi.noarch

Complete!

Rocky Linux 8 - AppStream
Name           Stream             Profiles                             Summary
php            7.2 [d]            common [d], devel, minimal           PHP scripting language
php            7.3                common [d], devel, minimal           PHP scripting language
php            7.4                common [d], devel, minimal           PHP scripting language

# On veut voir la liste des modules php qui peuvent être activés 
[mattox@web /]$ dnf module list php
(...)
Remis Modular repository for Enterprise Linux 8 - x86_64
Name           Stream             Profiles                             Summary
php            remi-7.2           common [d], devel, minimal           PHP scripting language
php            remi-7.3           common [d], devel, minimal           PHP scripting language
php            remi-7.4           common [d], devel, minimal           PHP scripting language
php            remi-8.0           common [d], devel, minimal           PHP scripting language
php            remi-8.1           common [d], devel, minimal           PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

# On active le module remi-7.4
[mattox@web /]$ sudo dnf module enable php:remi-7.4
(...)
Enabling module streams:
 php                                            remi-7.4

Complete!

# On installe les packets nécéssaires sur les deux machines
[mattox@web /]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
(...)
Complete!

# On verifie que le serveur Apache est activé au démarrage 
[mattox@web /]$ sudo systemctl is-enabled httpd
enabled

# On crée les dossiers "sites-available" et "sites-enabled"
[mattox@web ~]$ sudo mkdir /etc/httpd/sites-available
[mattox@web ~]$ sudo mkdir /etc/httpd/sites-enabled

# On crée aussi un répertoire où nos sites vont résider
[mattox@web ~]$ sudo mkdir /var/www/sub-domains/

# On ajoute une ligne à la configuration du fichier "/etc/httpd/conf/httpd.conf"
[mattox@web ~]$ sudo vi /etc/httpd/conf/httpd.conf
(...)
Include /etc/httpd/sites-enabled
IncludeOptional sites-enabled/*

# On créer le fichier de configuration de notre site
[mattox@web ~]$ sudo vi /etc/httpd/conf/sites-available/web.tp2.linux

<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/
  ServerName web.tp2.linux

  <Directory /var/www/sub-domains/web.tp2.linux/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

# On crée maintenant un lien vers le fichier dans "sites-enabled"
[mattox@web ~]$ sudo ln -s /etc/httpd/conf/sites-available/web.tp2.linux /etc/httpd/conf/sites-enabled/

# Nous créons le DocumentRoot
[mattox@web ~]$ sudo mkdir -p /var/www/sub-domains/web.tp2.linux/html

# On trouve notre fuseau horaire
[mattox@web ~]$ timedatectl | grep Time
                Time zone: Europe/Paris (CEST, +0200)

# On rempli le fichier php.ini avec notre fuseau horaire
[mattox@web ~]$ sudo vi /etc/opt/remi/php74/php.ini
(...)
;date.timezone = "Europe/Paris"

# Nous telechargons ensuite Nextcloud sur notre machine
[mattox@web ~]$ wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
(...)
2021-10-02 15:13:17 (340 KB/s) - ‘nextcloud-22.2.0.zip’ saved [159315304/159315304]

# Puis on le unzip
[mattox@web ~]$ unzip nextcloud-22.2.0.zip

# On copie le contenu de nextcloud dans notre DocumentRoot
[mattox@web ~]$ cd nextcloud
[mattox@web nextcloud]$ sudo cp -Rf * /var/www/sub-domains/web.tp2.linux/html/

# On s'assure que notre utilisateur "apache" possede le repertoire
[mattox@web web.tp2.linux]$ sudo chown -Rf apache:apache /var/www/sub-domains/web.tp2.linux/html

# On verifie donc que le dossier "html" appartient bien à notre utilisateur apache
[mattox@web /]$ ls -al /var/www/sub-domains/web.tp2.linux/html
total 120
drwxr-xr-x. 13 apache apache  4096 Oct  2 15:43 .
drwxr-xr-x.  3 root   root      18 Oct  2 14:39 ..
drwxr-xr-x. 43 apache apache  4096 Oct  2 15:43 3rdparty
drwxr-xr-x. 48 apache apache  4096 Oct  2 15:43 apps
-rw-r--r--.  1 apache apache 19327 Oct  2 15:43 AUTHORS
drwxr-xr-x.  2 apache apache    67 Oct  2 15:43 config
-rw-r--r--.  1 apache apache  3924 Oct  2 15:43 console.php
-rw-r--r--.  1 apache apache 34520 Oct  2 15:43 COPYING
drwxr-xr-x. 22 apache apache  4096 Oct  2 15:43 core
-rw-r--r--.  1 apache apache  5163 Oct  2 15:43 cron.php
-rw-r--r--.  1 apache apache   156 Oct  2 15:43 index.html
-rw-r--r--.  1 apache apache  3454 Oct  2 15:43 index.php
drwxr-xr-x.  6 apache apache   125 Oct  2 15:43 lib
-rw-r--r--.  1 apache apache   283 Oct  2 15:43 occ
drwxr-xr-x.  2 apache apache    23 Oct  2 15:43 ocm-provider
drwxr-xr-x.  2 apache apache    55 Oct  2 15:43 ocs
drwxr-xr-x.  2 apache apache    23 Oct  2 15:43 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  2 15:43 public.php
-rw-r--r--.  1 apache apache  5340 Oct  2 15:43 remote.php
drwxr-xr-x.  4 apache apache   133 Oct  2 15:43 resources
-rw-r--r--.  1 apache apache    26 Oct  2 15:43 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  2 15:43 status.php
drwxr-xr-x.  3 apache apache    35 Oct  2 15:43 themes
drwxr-xr-x.  2 apache apache    43 Oct  2 15:43 updater
-rw-r--r--.  1 apache apache   422 Oct  2 15:43 version.php
```

📁 **On récupere le fichier `httpd.conf` qu'on renomme `httpd2.conf`** => [httpd2.conf](./ressources/httpd2.conf)

📁 **On récupere le fichier `web.tp2.linux`** => [web.tp2.linux](./ressources/web.tp2.linux)

### B. Base de données

#### 🌞 Install de MariaDB sur db.tp2.linux

```bash
# On commence par installer les ressources nécéssaires
[mattox@db ~]$ sudo dnf install mariadb-server
(...)
Complete !

# On active le service mariadb
[mattox@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service
[mattox@db ~]$ sudo systemctl start mariadb

# On configure MariaDB
[mattox@db ~]$ mysql_secure_installation
(...)
Enter current password for root (enter for none):
OK, successfully used password, moving on...
(...)
Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
(...)
Remove anonymous users? [Y/n] Y
 ... Success!
(...)
Disallow root login remotely? [Y/n] Y
 ... Success!
(...)
Remove test database and access to it? [Y/n]
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!
(...)
Reload privilege tables now? [Y/n]
 ... Success!

Cleaning up...

All done!  If you ve completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
(...)

# On regarde quel port utilise mariadb
[mattox@db ~]$ ss -altpn
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
LISTEN        0             128                           [::]:22                         [::]:*
LISTEN        0             80                               *:3306                          *:*
# Le port 22 est utilisé par la connexion SSH
# MariaDB utilise donc le port 3306

# On ouvre donc le port 3306 sur le firewall
[mattox@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for mattox:
success
[mattox@db ~]$ sudo firewall-cmd --reload
success
```

#### 🌞 Préparation de la base pour NextCloud

```mysql
# On cree un utilisateur dans la base de données avec un mot de passe
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

# Création de la base de donnée qui sera utilisée par NextCloud
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.001 sec)

# Actualisation des privilèges
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

#### 🌞 Exploration de la base de données

```myqsl
# On se connecte à la base de données depuis web.tp2.linux
[mattox@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
# Et on rentre le mot de passe 'meow'

# On affiche les bases de données
MariaDB [(none)]> SHOW DATABASES
    -> ;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.002 sec)

# On utilise donc la base de données "nextcloud"
MariaDB [(none)]> USE nextcloud;
Database changed

# Et on regardes les Tables de 'nextcloud', qui est actuellement vide
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.002 sec)

# Pour afficher la liste des utilisateurs de la base de données,
# il faut se connecter en root sur db.tp2.linux
[mattox@db ~]$ sudo mysql -u root -p

# On peut donc afficher la liste des utilisateurs
MariaDB [(none)]> select user from mysql.user;
+-----------+
| user      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.001 sec)
```

### C. Finaliser l'installation de NextCloud

#### 🌞 sur votre PC

```bash
# Pour modifier notre fichier hosts, on ouvre un PowerShell en mode adminstrateur
# pour effectuer ces commandes
PS C:\Users\matte> notepad C:\Windows\System32\drivers\etc\hosts
127.0.0.1 localhost
::1 localhost
10.102.1.11 web.tp2.linux
# On lui ajoute la derniere ligne pour qu'il puisse acceder à l'adresse "http://web.tp2.linux"

# On accede donc à l'ecran d'acceuil de NextCloud à l'adresse "http://web.tp2.linux"
PS C:\Users\matte> curl http://web.tp2.linux

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>
(...)
```
Nous devons saisir les informations dans Nextcloud

`repertoire des données` : `/var/www/sub-domains/web.tp2.linux/html/data`

`utilisateur de la base de donnée` : `nextcloud`

`mot de passe de l'utilisateur` : `meow`

`nom de la base de donnée` : `nextcloud`

`hote de la base de donnée` : `10.102.1.12`

#### 🌞 Exploration de la base de données

```mysql
# On se connecte à la base de donnée
[mattox@db ~]$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.
(...)

# On peut voir que 108 tables ont été crée durant la configuration de nextcloud
MariaDB [nextcloud]> SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'nextcloud';
+----------+
| COUNT(*) |
+----------+
|      108 |
+----------+
1 row in set (0.003 sec)
```

#### 🌞 Tableau

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80           | toutes             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306           | `10.102.1.11 `            |

