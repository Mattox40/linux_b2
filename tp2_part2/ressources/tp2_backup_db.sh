#!/bin/bash
# Sauvegarde de base de données
# mattox - 13/10/2021

backup=$(date +tp2_backup_db_%y%m%d_%H%M%S)
backup_fullpath=$(pwd)/${backup}
database=$2
destination=$1
mysqldump --user=root --password=root --databases ${database} > ${backup}.sql
tar -cvzf "${backup_fullpath}.tar.gz" "${backup}.sql"
rsync -av --remove-source-files "${backup_fullpath}.tar.gz" "${destination}"
rm ${backup}.sql
