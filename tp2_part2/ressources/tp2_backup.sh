#!/bin/bash
# Sauvegarde de dossier
# mattox - 12/10/2021

backup=$(date +tp2_backup_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="$(pwd)/${backup}"
destination=$1
dir_to_archive=$2
tar -cvzf "${backup_fullpath}" "${dir_to_archive}"
rsync -av --remove-source-files "${backup_fullpath}" "${destination}"
