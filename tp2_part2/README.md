# TP2 pt. 2 : Maintien en condition opérationnelle

# I. Monitoring

## 2. Setup

#### 🌞 Setup Netdata

```bash
# On se connecte en tant que root sur web.tp2.linux
[mattox@web ~]$ sudo su -
[sudo] password for mattox:
Last login: Wed Sep 15 15:42:45 CEST 2021 on tty1
[root@web ~]$

# On installe NetData
[root@web ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
(...)

# On quitte la session de root
[root@web ~]$ exit
logout
[mattox@web ~]$

# On fais la même manipulation sur db.tp2.linux
[mattox@db ~]$ sudo su -
[sudo] password for mattox:
Last login: Wed Oct  6 16:11:28 CEST 2021 on pts/0
[root@db ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
(...)
[root@db ~]$ exit
logout
[mattox@db ~]$
```

#### 🌞 Manipulation du service Netdata

```bash
# Le service netdata a été crée et est actif
[mattox@web ~]$ sudo systemctl status netdata
[sudo] password for mattox:
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 14:47:48 CEST; 10min ago
(...)

# Netdata est activé au boot de la machine
[mattox@web ~]$ sudo systemctl is-enabled netdata
enabled

# On peux voir que le port d'écoute de Netdata est le port 19999 
[mattox@web ~]$ ss -alnpt
State      Recv-Q     Send-Q          Local Address:Port            Peer Address:Port     Process
LISTEN     0          128                   0.0.0.0:22                   0.0.0.0:*
LISTEN     0          128                 127.0.0.1:8125                 0.0.0.0:*
LISTEN     0          128                   0.0.0.0:19999                0.0.0.0:*
LISTEN     0          128                         *:80                         *:*
LISTEN     0          128                      [::]:22                      [::]:*
LISTEN     0          128                     [::1]:8125                    [::]:*
LISTEN     0          128                      [::]:19999                   [::]:*

# On autorise ce port sur le firewall
[mattox@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[mattox@web ~]$ sudo firewall-cmd --reload
success
```

#### 🌞 Setup Alerting

```bash
# On edite health_alarm_notify.conf avec notre webhook
[mattox@web /]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
[sudo] password for mattox:
Copying '/opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf' to '/opt/netdata/etc/netdata/health_alarm_notify.conf' ...
Editing '/opt/netdata/etc/netdata/health_alarm_notify.conf' ...
(...)
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897110052949860355/XJeAT-Wv6FPI7h-zGZrsyaKDmvio3EXsHVe-1rSRllfq3-0MiwbNERld_DitItTmKSgc"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="general"
(...)

# On restart netdata
[mattox@web /]$ sudo systemctl restart netdata

# On verifie le bon fonctionnement de l'alerting
[mattox@web /]$ sudo su -s /bin/bash netdata

bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1

bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
(...)
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-11 16:23:51: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'general'
# On a une réponse 200, le resultat est donc bon
# On peux voir qu'on as aussi reçu les résultats sur Discord

[mattox@web ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

#### 🌞 Config alerting

```bash
# On cree une nouvelle alerte pour le remplissage de 50% de la RAM
[mattox@web netdata]$ cd /opt/netdata/etc/netdata/
[mattox@web netdata]$ ./edit-config health.d/cpu.conf
[mattox@web netdata]$ cat health.d/cpu.conf
alarm: ram_usage
on: system.ram
lookup: average -1m percentage of used
units: %
every: 1m
warn: $this > 50
crit: $this > 90
info: The percentage of RAM being used by the system.

# On restart netdata
[mattox@web netdata]$ sudo systemctl restart netdata

# On installe le packet stress
[mattox@web netdata]$ sudo dnf install -y stress
(...)
Installed:
  stress-1.0.4-24.el8.x86_64

Complete!

# On applique le packet pour remplir artifiellement la RAM
[mattox@web netdata]$ stress -c 100 -t 20s
stress: info: [28900] dispatching hogs: 100 cpu, 0 io, 0 vm, 0 hdd
stress: info: [28900] successful run completed in 20s

# Et nous pouvons voir que sur discord, nous recevons les messages
# d'alertes
# embed discord :
System five-minute load average. High system five-minute load average. A constantly high value indicates that your system is overloaded. It includes both CPU and I/O demand. You might want to check per-process CPU/disk usage to find the top consumers.
**system.load**
load
```

# II. Backup

## 2. Partage NFS

#### 🌞 Setup environnement

```bash
# On cree un dossier /srv/backups/
[mattox@backup ~]$ sudo mkdir /srv/backups/

# On cree un sous-dossier pour chaque machine du parc
[mattox@backup ~]$ sudo mkdir /srv/backups/web.tp2.linux/
[mattox@backup ~]$ sudo mkdir /srv/backups/db.tp2.linux/

# Donner les privileges aux sous-dossiers
[mattox@backup ~]$ sudo chmod 777 /srv/backups/web.tp2.linux/
[mattox@backup ~]$ sudo chmod 777 /srv/backups/db.tp2.linux/
```

#### 🌞 Setup partage NFS

```bash
# On commence par installer le packet nfs-utils
[mattox@backup ~]$ sudo dnf -y install nfs-utils
(...)
Complete!

# On insere notre nom de domaine
[mattox@backup ~]$ sudo vi /etc/idmapd.conf
[mattox@backup ~]$ sudo cat /etc/idmapd.conf | grep Domain
Domain = tp2.linux

# On ajoute nos deux fichiers pour le partage NFS des deux machines
[mattox@backup ~]$ sudo vi /etc/exports
[mattox@backup ~]$ cat /etc/exports
/srv/backup/web.tp2.linux/ 10.102.1.11/24(rw)
/srv/backup/db.tp2.linux/ 10.102.1.12/24(rw)

# On restart nfs-server
[mattox@backup /]$ sudo systemctl restart nfs-server

[mattox@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

# On ajoute NFS au firewall
[mattox@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[mattox@backup ~]$ sudo firewall-cmd --runtime-to-permanent
success
[mattox@backup ~]$ sudo firewall-cmd --reload
success
```

#### 🌞 Setup points de montage sur `web.tp2.linux`

```bash
# On installe le packet NFS
[mattox@web ~]$ sudo dnf -y install nfs-utils
(...)
Complete!

# On ajoute notre nom de domaine
[mattox@web ~]$ sudo cat /etc/idmapd.conf | grep Domain
Domain = tp2.linux

# On monte notre partage NFS
[mattox@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backups/web.tp2.linux /srv/backup

# On verifie que la partition est bien monté
[mattox@web ~]$ mount | grep backup
backup.tp2.linux:/srv/backups/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=262144,wsize=262144,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)

# On a pour 6G, 2,3G sont utilisés donc il reste de la place 
[mattox@web ~]$ df -h | grep backup
backup.tp2.linux:/srv/backups/web.tp2.linux  6.2G  2.3G  4.0G  36% /srv/backup

# On crée un fichier pour montrer que nous ayons les droits d'écriture 
[mattox@web ~]$ touch /srv/backup/fichierdetest
[mattox@web ~]$ ls -al /srv/backup/ | grep fichierdetest
-rw-rw-r--. 1 mattox mattox  0 Oct 12 17:04 fichierdetest

# On monte la partition automatiquement quand le système démarre
[mattox@web ~]$ sudo vi /etc/fstab
[mattox@web ~]$ cat /etc/fstab | grep backup
backup.tp2.linux:/srv/backups/web.tp2.linux     nfs     defaults        0 0
```

#### 🌟 BONUS : partitionnement avec LVM

```bash
# On a ajouté un disque à backup.tp2.linux, qui est donc sdb
[mattox@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    5G  0 disk
sr0          11:0    1 1024M  0 rom

# On crée un PV
[mattox@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for mattox:
  Physical volume "/dev/sdb" successfully created.

# Puis un VG
[mattox@backup ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created

# On ajoute le PV au VG
[mattox@backup ~]$ sudo lvcreate -L 5G data -n ma_data
  Volume group "data" has insufficient free space (1279 extents): 1280 required.

# Cree un LV de 4.9G car j'ai choisi un disque de 5Go
[mattox@backup ~]$ sudo lvcreate -L 4.9G data -n ma_data
  Rounding up size to full physical extent 4.90 GiB
  Logical volume "ma_data" created.

# On vérifie que le Logical Volume apparait bien
[mattox@backup ~]$ sudo lvs
  LV      VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  ma_data data -wi-a-----   4.90g
  root    rl   -wi-ao----  <6.20g
  swap    rl   -wi-ao---- 820.00m

# On formate notre partition
[mattox@backup ~]$ sudo mkfs -t ext4 /dev/data/ma_data
(...)
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done

# On monte cette partition sur /srv/backups
[mattox@backup ~]$ sudo mount /dev/data/ma_data /srv/backups
[mattox@backup ~]$ mount | grep /srv/backups
/dev/mapper/data-ma_data on /srv/backups type ext4 (rw,relatime,seclabel)

# On le defini au montage automatique de le machine
[mattox@backup ~]$ sudo vi /etc/fstab
[mattox@backup ~]$ sudo cat /etc/fstab | grep /dev/data/ma_data
/dev/data/ma_data /srv/backups ext4 defaults 0 0
```

## 3. Backup de fichiers

#### 🌞 Rédiger le script de backup `/srv/tp2_backup.sh`

```bash
# On crée le script /srv/tp2_backup.sh
[mattox@backup ~]$ sudo vi /srv/tp2_backup.sh
[mattox@backup ~]$ sudo cat /srv/tp2_backup.sh
#!/bin/bash
# Sauvegarde de dossier
# mattox - 12/10/2021

backup=$(date +tp2_backup_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="$(pwd)/${backup}"
destination=$1
dir_to_archive=$2
tar -cvzf "${backup_fullpath}" "${dir_to_archive}"
rsync -av --remove-source-files "${backup_fullpath}" "${destination}"

# On ajoute les permissions à root de lancer le script
[mattox@backup ~]$ sudo chmod 700 /srv/tp2_backup.sh
```


=> [tp2_backup.sh](./ressources/tp2_backup.sh)

#### 🌞 Tester le bon fonctionnement

```bash
# On crée un dossier "toto" qui sera celui à backup 
# et un fichier "fichier" 
[mattox@backup ~]$ sudo mkdir toto
[mattox@backup ~]$ sudo touch /toto/fichier

# Puis on crée un dossier /dir/ qui sera la destination 
# de notre dossier à backup
[mattox@backup ~]$ sudo mkdir dir

# On lance notre script
[mattox@backup ~]$ sudo bash -x /srv/tp2_backup.sh dir toto
++ date +tp2_backup_%y%m%d_%H%M%S.tar.gz
+ backup=tp2_backup_211013_211708.tar.gz
++ pwd
+ backup_fullpath=/home/mattox/tp2_backup_211013_211708.tar.gz
+ destination=dir
+ dir_to_archive=toto
+ tar -cvzf /home/mattox/tp2_backup_211013_211708.tar.gz toto
toto/
toto/fichier
+ rsync -av --remove-source-files /home/mattox/tp2_backup_211013_211708.tar.gz dir
sending incremental file list
tp2_backup_211013_211708.tar.gz

sent 259 bytes  received 43 bytes  604.00 bytes/sec
total size is 142  speedup is 0.47
# Notre script c'est bien passé

# On peut voir qu'on as notre archive dans /dir/
[mattox@backup ~]$ ls /dir/
tp2_backup_211013_163410.tar.gz

# Si on essaye de le restaurer
[mattox@backup /]$ sudo tar -xvf /dir/tp2_backup_211013_163410.tar.gz -C /dir/
toto/
toto/fichier
# On peut voir que le dossier "toto" et le fichier sont encore présents

# Et on peux recuperer notre dossier toto où notre 
# fichier "fichier" est bien présent
[mattox@backup ~]$ ls /dir/toto
fichier
```

#### 🌟 BONUS

```bash
# Notre script prends maintenant en compte les 5 archives les plus récentes, et on peux y ajouter autant de dossiers que l'ont veux
[mattox@backup ~]$ sudo cat /srv/tp2_backup.sh
#!/bin/bash
# Sauvegarde de dossier
# mattox - 12/10/2021

backup=$(date +tp2_backup_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="$(pwd)/${backup}"
destination=$1
dir_to_archive=$2
tar -cvzf "${backup_fullpath}" "${dir_to_archive}"
rsync -av --remove-source-files "${backup_fullpath}" "${destination}"
ls -tp "${destination}" | grep -v '/$' | tail -n +6 | xargs -I {} rm -- ${destination}/{}
```

## 4. Unité de service

#### 🌞 Créer une unité de service pour notre backup

```bash
# On cree le fichier "tp2_backup.service"
[mattox@backup ~]$ sudo touch /etc/systemd/system/tp2_backup.service

# On le modifie pour y mettre nos informations
[mattox@backup ~]$ sudo vi /etc/systemd/system/tp2_backup.service
[mattox@backup ~]$ cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /home/mattox/dir/ /home/mattox/toto/
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

#### 🌞 Tester le bon fonctionnement

```bash
# On ajoute un service, on doit donc reload le daemon
[mattox@backup ~]$ sudo systemctl daemon-reload

# On demarre le service créée
[mattox@backup ~]$ sudo systemctl start tp2_backup

# On regarde dans "dir" et on peut voir une nouvelle archive récemment créée 
[mattox@backup ~]$ ls dir/
tp2_backup_211013_213016.tar.gz  tp2_backup_211013_214517.tar.gz
```

### B. Timer

#### 🌞 Créer le timer associé à notre tp2_backup.service

```bash
# On crée un fichier "tp2_backup.timer"
[mattox@backup ~]$ sudo touch /etc/systemd/system/tp2_backup.timer

# et on intègre nos informations 
[mattox@backup ~]$ sudo vi /etc/systemd/system/tp2_backup.timer
[mattox@backup ~]$ cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

#### 🌞 Activez le timer

```bash
# On relance le daemon à chaque nouveau service
[mattox@backup ~]$ sudo systemctl daemon-reload

# On démarre le timer
[mattox@backup ~]$ sudo systemctl start tp2_backup.timer

# On active le timer au démarrage
[mattox@backup ~]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.

# Le timer est actif et paramétré pour etre actif dès que le système boot
[mattox@backup ~]$ sudo systemctl is-active tp2_backup.timer
active
[mattox@backup ~]$ sudo systemctl is-enabled tp2_backup.timer
enabled
```

### 🌞 Tests !

```bash
# On peut voir la backup s'éxécute correctement car le dossier 
# /dir/ contient toutes les backups de chaque minutes
[mattox@backup ~]$ ls -al dir/
total 40
drwxr-xr-x. 2 root   root   4096 Oct 13 22:00 .
drwx------. 5 mattox mattox  118 Oct 13 21:42 ..
-rw-r--r--. 1 root   root    153 Oct 13 21:30 tp2_backup_211013_213016.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 21:45 tp2_backup_211013_214517.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 21:54 tp2_backup_211013_215430.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 21:55 tp2_backup_211013_215521.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 21:56 tp2_backup_211013_215633.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 21:57 tp2_backup_211013_215733.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 21:58 tp2_backup_211013_215833.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 21:59 tp2_backup_211013_215933.tar.gz
-rw-r--r--. 1 root   root    153 Oct 13 22:00 tp2_backup_211013_220033.tar.gz
```

### C. Contexte

```bash
# On copie notre tp2_backup.sh sur web
[mattox@web ~]$ sudo vi /srv/tp2_backup.sh
[mattox@web ~]$ sudo cat /srv/tp2_backup.sh
#!/bin/bash
# Sauvegarde de dossier
# mattox - 12/10/2021

backup=$(date +tp2_backup_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="$(pwd)/${backup}"
destination=$1
dir_to_archive=$2
tar -cvzf "${backup_fullpath}" "${dir_to_archive}"
rsync -av --remove-source-files "${backup_fullpath}" "${destination}"

# On crée un service tp2_backup.service
# On programme notre service pour sauvegarder /var/www/sub-domains/web.tp2.linux/html/
# dans le partage NFS /srv/backup
[mattox@web ~]$ sudo vi /etc/systemd/system/tp2_backup.service
[mattox@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/sub-domains/web.tp2.linux/html/
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target

# On crée aussi le timer tp2_backup.timer
# On modifie le OnCalendar pour qu'il se déclenche tous les jours à 3h15
[mattox@web ~]$ sudo vi /etc/systemd/system/tp2_backup.timer
[mattox@web ~]$ sudo cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target

# Nous avons créée 2 nouveaux services
[mattox@web ~]$ sudo systemctl daemon-reload

# On voit que le timer s'activera à 03h15 le lendemain
# Et qu'il activera tp2_backup.service
[mattox@web ~]$ sudo systemctl list-timers | grep tp2_backup
Fri 2021-10-15 03:15:00 CEST  13h left n/a                           n/a       tp2_backup.timer             tp2_backup.service

# On test notre tp2_backup.service
[mattox@web ~]$ sudo systemctl start tp2_backup

# On voit que notre programme a bien enregistrer 
# notre archive dans le partage NFS
[mattox@web ~]$ ls -al /srv/backup/ | grep tp2
-rw-r--r--. 1 root root 209121203 Oct 14 13:22 tp2_backup_211014_132219.tar.gz
```

=> [tp2_backup.timer](./ressources/tp2_backup.timer)

=> [tp2_backup.service](./ressources/tp2_backup.service)

## 5. Backup de base de données

#### 🌞 Création d'un script `/srv/tp2_backup_db.sh`

```bash
# On crée le script tp2_backup_db.sh sur db.tp2.linux
[mattox@db ~]$ sudo vi /srv/tp2_backup_db.sh
[mattox@db ~]$ cat /srv/tp2_backup_db.sh
#!/bin/bash
# Sauvegarde de base de données
# mattox - 13/10/2021

backup=$(date +tp2_backup_db_%y%m%d_%H%M%S)
backup_fullpath=$(pwd)/${backup}
database=$2
destination=$1
mysqldump --user=root --password=root --databases ${database} > ${backup}.sql
tar -cvzf "${backup_fullpath}.tar.gz" "${backup}.sql"
rsync -av --remove-source-files "${backup_fullpath}.tar.gz" "${destination}"
rm ${backup}.sql
```

=> [tp2_backup_db.sh](./resources/tp2_backup_db.sh)

#### 🌞 Restauration

```bash
# On essaye le script sur un dossier "test"
[mattox@db ~]$ sudo mkdir toto

# Et on utilise la commande sur la base de données nextcloud
[mattox@db ~]$ sudo bash -x /srv/tp2_backup_db.sh test/ nextcloud
[mattox@db ~]$ sudo bash -x /srv/tp2_backup_db.sh test/ nextcloud
[sudo] password for mattox:
++ date +tp2_backup_db_%y%m%d_%H%M%S
+ backup=tp2_backup_db_211014_152659
++ pwd
+ backup_fullpath=/home/mattox/tp2_backup_db_211014_152659
+ database=nextcloud
+ destination=test/
+ mysqldump --user=root --password=root --databases nextcloud
+ tar -cvzf /home/mattox/tp2_backup_db_211014_152659.tar.gz tp2_backup_db_211014_152659.sql
tp2_backup_db_211014_152659.sql
+ rsync -av --remove-source-files /home/mattox/tp2_backup_db_211014_152659.tar.gz test/
sending incremental file list
tp2_backup_db_211014_152659.tar.gz

sent 38,478 bytes  received 43 bytes  25,680.67 bytes/sec
total size is 38,354  speedup is 1.00
+ rm tp2_backup_db_211014_152659.sql

# On a donc notre archive dans le dossier "test"
[mattox@db ~]$ ls -al test/
total 40
drwxr-xr-x. 2 root   root      48 Oct 14 15:27 .
drwx------. 4 mattox mattox   129 Oct 14 15:27 ..
-rw-r--r--. 1 root   root   38354 Oct 14 15:26 tp2_backup_db_211014_152659.tar.gz

# On restaure l'archive
[mattox@db ~]$ sudo tar -xvf test/tp2_backup_db_211014_152659.tar.gz -C test/
tp2_backup_db_211014_152659.sql

# On obtient un fichier .sql
[mattox@db ~]$ ls -al test/ | grep sql
-rw-r--r--. 1 root   root   193356 Oct 14 15:26 tp2_backup_db_211014_152659.sql

# Pour l'exemple on va supprimer une table "oc_accounts" 
# de la base "nextcloud" pour essayer de voir 
# si la backup va rétablir la table
[mattox@db ~]$ mysql -u root -p
(...)
MariaDB [(none)]> use nextcloud;
Database changed

# "oc_accounts" se situe dans la base de donnée "nextcloud"
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |

# On delete la table "oc_accounts"
MariaDB [nextcloud]> DROP TABLE oc_accounts;
Query OK, 0 rows affected (0.016 sec)

# Elle n'est plus dans la base de donnée
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts_data            |
| oc_activity                 |
(...)

# On insere notre backup de la base de données dans nextcloud
[mattox@db ~]$ sudo mysql -u root -p nextcloud < test/tp2_backup_db_211014_152659.sql

# On se reconnecte et on regarde nos tables
[mattox@db ~]$ mysql -u root -p

MariaDB [(none)]> use nextcloud
Database changed

# On regarde nos tables, "oc_accounts" est bien revenu
# Notre sauvegarde a donc marché
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |
```

#### 🌞 Unité de service

```bash
# On crée le service "tp2_backup_db.service"
[mattox@db ~]$ sudo vi /etc/systemd/system/tp2_backup_db.service

# On sauvegarde la base de donnée nextcloud sur le partage NFS 
[mattox@db ~]$ sudo cat /etc/systemd/system/tp2_backup_db.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup_db.sh /srv/backup nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target

# On crée aussi le timer tp2_backup_db.timer
[mattox@web ~]$ sudo vi /etc/systemd/system/tp2_backup_db.timer

# On programme notre timer pour qu'il se lance tous les jours à 3h30
[mattox@web ~]$ sudo cat /etc/systemd/system/tp2_backup_db.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup_db.service

[Timer]
Unit=tp2_backup_db.service
OnCalendar=*-*-* 3:30:00

[Install]
WantedBy=timers.target

# On peux voir que notre timer s'activera à 3:30 et qu'il activera tp2_backup_db.service
[mattox@db ~]$ sudo systemctl list-timers | grep tp2_backup
Fri 2021-10-15 03:30:00 CEST  9h left    n/a                           n/a       tp2_backup_db.timer          tp2_backup_db.service

# Donc notre timer marche et nous avons prouvé que le tp2_backup_db.service marche lui aussi
```

=> [tp2_backup_db.service]('./ressources/tp2_backup_db.service')

=> [tp2_backup_db.timer]('./ressources/tp2_backup_db.timer')

## 6. Petit point sur la backup

# III. Reverse Proxy

## 2. Setup simple

#### 🌞 Installer NGINX

```bash
# On installe le packet "epel-release"
[mattox@front ~]$ sudo dnf install epel-release
(...)
Installed:
  epel-release-8-13.el8.noarch

Complete!

# On installe le packet "NGINX"
[mattox@front ~]$ sudo dnf install nginx
(...)
Complete!
```

#### 🌞 Tester !

```bash
# On lance le service nginx
[mattox@front ~]$ sudo systemctl start nginx

# On le configure pour qu'il démarre au boot du systeme
[mattox@front ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

# On peut voir que nginx utilise le port 80
[mattox@front ~]$ ss -alnpt
State        Recv-Q       Send-Q             Local Address:Port              Peer Address:Port       Process
LISTEN       0            128                      0.0.0.0:80                     0.0.0.0:*
LISTEN       0            128                      0.0.0.0:22                     0.0.0.0:*
LISTEN       0            128                    127.0.0.1:8125                   0.0.0.0:*
LISTEN       0            128                      0.0.0.0:19999                  0.0.0.0:*
LISTEN       0            128                         [::]:80                        [::]:*
LISTEN       0            128                         [::]:22                        [::]:*
LISTEN       0            128                        [::1]:8125                      [::]:*
LISTEN       0            128                         [::]:19999                     [::]:*

# On l'autorise donc sur le firewall
[mattox@front ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
success
[mattox@front ~]$ sudo firewall-cmd --reload
success

# On curl depuis mon PC
PS C:\Users\matte> curl 10.102.1.14:80

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
                    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
# Code 200, c'est bon, on peux donc avoir le serveur NGINX
```

#### 🌞 Explorer la conf par défaut de NGINX

```bash
# On cherche dans la conf pour reperer l'utilisateur de nginx
[mattox@front ~]$ sudo cat /etc/nginx/nginx.conf | grep user
user nginx;
# l'utilisateur est donc "nginx"

# on repere le bloc server {}
[mattox@front ~]$ sudo cat /etc/nginx/nginx.conf
(...)
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
(...)

# le fichier de conf principal inclut d'autres fichiers de conf
# on les met donc en évidence
[mattox@front ~]$ sudo cat /etc/nginx/nginx.conf | grep *.conf
include /usr/share/nginx/modules/*.conf;
    include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
#        include /etc/nginx/default.d/*.conf;
```

#### 🌞 Modifier la conf de NGINX

```bash
# Le fichier /etc/hosts contient nos 3 autres machines
[mattox@front ~]$ sudo cat /etc/hosts | grep tp2.linux
10.102.1.11 web.tp2.linux
10.102.1.12 db.tp2.linux
10.102.1.13 backup.tp2.linux

# On enleve le server {} de la conf principal
[mattox@front ~]$ sudo vim /etc/nginx/nginx.conf

# et on cree notre fichier de conf
[mattox@front ~]$ sudo touch /etc/nginx/conf.d/web.tp2.linux.conf
[mattox@front ~]$ sudo vim /etc/nginx/conf.d/web.tp2.linux.conf
[mattox@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    listen 80;
    server_name web.tp2.linux;
    location / {
        proxy_pass http://web.tp2.linux;
    }
}
```

## 3. Bonus HTTPS

#### 🌟 Générer la clé et le certificat pour le chiffrement

```bash
# On se place à un endroit où on peut écrire
[mattox@front ~]$ cd ~

[mattox@front ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
..................+++++
.........+++++
writing new private key to 'server.key'
(...)
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Aquitaine
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:Ynov
Organizational Unit Name (eg, section) []:
# On met surtout le nom de notre serveur
Common Name (eg, your name or your servers hostname) []:web.tp2.linux
Email Address []:

# On déplace la clé et le certificat dans les dossiers standards de Rocky Linux
[mattox@front ~]$ sudo mv server.key /etc/pki/tls/private/web.tp2.linux.key
[mattox@front ~]$ sudo mv server.crt /etc/pki/tls/certs/web.tp2.linux.crt

# Setup des permissions administratives
[mattox@front ~]$ sudo chown root:root /etc/pki/tls/private/web.tp2.linux.key
[mattox@front ~]$ sudo chown root:root /etc/pki/tls/certs/web.tp2.linux.crt
[mattox@front ~]$ sudo chmod 400 /etc/pki/tls/private/web.tp2.linux.key
[mattox@front ~]$ sudo chmod 644 /etc/pki/tls/certs/web.tp2.linux.crt
```

#### 🌟 Modifier la conf de NGINX

```bash
# On modifie notre fichier de conf pour 
[mattox@front ~]$ sudo vim /etc/nginx/conf.d/web.tp2.linux.conf
[mattox@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    # On écoute donc sur le port 443 
    listen 443;
    server_name web.tp2.linux;

    ssl on; # on active le chiffrement
    # On ajoute le certificat et la clé
    ssl_certificate /etc/pki/tls/certs/web.tp2.linux.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp2.linux.key;

    location / {
        proxy_pass http://web.tp2.linux;
    }
}

# On vérifie si notre fichier de conf est correct
[mattox@front ~]$ sudo nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful

# On ouvre le port 443 sur le firewall
[mattox@front ~]$ sudo firewall-cmd --zone=public --add-port=443/tcp --permanent
[mattox@front ~]$ sudo firewall-cmd --reload
```

#### 🌟 TEST

```bash
# On change donc notre fichier hosts de notre PC
# Car notre site réponds maintenant sous front.tp2.linux
10.102.1.14 web.tp2.linux

# On peut voir qu'on a effectivement une sécurité 
PS C:\Users\matte> curl https://web.tp2.linux
curl : La connexion sous-jacente a été fermée: Impossible d établir une relation de confiance pour le canal sécurisé
SSL/TLS.

# Sur db on modifie le fichier hosts pour notre test
[mattox@db ~]$ cat /etc/hosts | grep web
10.102.1.14 web.tp2.linux

# En enlevant cette sécurité on tombe bien sur notre site
[mattox@db ~]$ curl -k https://web.tp2.linux
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```

# IV. Firewalling

## 2. Mise en place

### A. Base de données

#### 🌞 Restreindre l'accès à la base de données `db.tp2.linux`

```bash
# Pour le moment on a juste une zone public
[mattox@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 3306/tcp 19999/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

# On selectionne la zone drop par défaut pour refuser tous les paquets
[mattox@db ~]$ sudo firewall-cmd --set-default-zone=drop
success

# On y ajoute notre interface
[mattox@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
success

# Ensuite on crée une zone ssh pour autoriser le trafic depuis 10.102.1.1
[mattox@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success

# 10.102.1.1 sera l'IP autorisée
[mattox@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success

# On ajoute uniquement le port 22 
[mattox@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success

# On ajoute la zone db pour gerer les echanges avec notre serveur web
[mattox@db ~]$ sudo firewall-cmd --new-zone=db --permanent
success

# Seul le serveur web peut communiquer avec notre base de données
[mattox@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32 --permanent
success

# On ajoute le port 3306 qui corresponds au port de notre base de données
[mattox@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
success
```

#### 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`

```bash
# On a donc 3 zones actives
[mattox@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32

# Avec la zone drop par défault piour que toutes les requetes soit refusé
[mattox@db ~]$ sudo firewall-cmd --get-default-zone
drop

# Et on montre le résultat de nos zones 
[mattox@db ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
(...)
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
(...)
[mattox@db ~]$ sudo firewall-cmd --list-all --zone=db
db (active)
(...)
  sources: 10.102.1.11/32
  services:
  ports: 3306/tcp
(...)
[mattox@db ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
(...)
```

### B. Serveur Web

#### 🌞 Restreindre l'accès au serveur Web `web.tp2.linux`

```bash
# On configure la zone drop par défaut
[mattox@web ~]$ sudo firewall-cmd --set-default-zone=drop
success

# On y ajoute l'interface enp0s8
[mattox@web ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent
success

# On cree la zone ssh
[mattox@web ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success

# Seule l'IP 10.102.1.1 sera autorisé
[mattox@web ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success

# Ainsi que le port 22 (ssh)
[mattox@web ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success

# On cree une nouvelle zone web
[mattox@web ~]$ sudo firewall-cmd --new-zone=web --permanent
success

# Seule l'addresse IP de front sera autorisé
[mattox@web ~]$ sudo firewall-cmd --zone=web --add-source=10.102.1.14/32 --permanent
success

# Ainsi que seul le port 80
[mattox@web ~]$ sudo firewall-cmd --zone=web --add-port=80/tcp --permanent
success
```

#### 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`

```bash
# On a donc 3 zones actives 
[mattox@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
web
  sources: 10.102.1.14/32

# Avec la zone drop par défaut
[mattox@web ~]$ sudo firewall-cmd --get-default-zone
drop

# et on affiche toute la conf du firewall
[mattox@web ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
(...)
  interfaces: enp0s3 enp0s8
(...)
[mattox@web ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
(...)
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
(...)
[mattox@web ~]$ sudo firewall-cmd --list-all --zone=web
web (active)
(...)
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
(...)
```

### C. Serveur de backup

#### 🌞 Restreindre l'accès au serveur de backup backup.tp2.linux

```bash
# On configure la zone drop par défaut
[mattox@backup ~]$ sudo firewall-cmd --set-default-zone=drop
success

# On ajoute l'interface
[mattox@backup ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent
success

# On cree une zone ssh
[mattox@backup ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success

# Seul 10.102.1.1 et le port 22 seront autorisés
[mattox@backup ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[mattox@backup ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success

# On cree une zone nfs
[mattox@backup ~]$ sudo firewall-cmd --new-zone=nfs --permanent
success

# Seules web.tp2.linux et db.tp2.linux seront autorisés
[mattox@backup ~]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.11/32 --permanent
success
[mattox@backup ~]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.12/32 --permanent
success

# Et seule le port 19999 sera autorisé
[mattox@backup ~]$ sudo firewall-cmd --zone=nfs --add-port=19999/tcp --permanent
success

# On ajoute le service nfs
[mattox@backup ~]$ sudo firewall-cmd --zone=nfs --add-service=nfs --permanent
success
```

#### 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`

```bash
# Nous avons donc 3 zones actives
[mattox@backup ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
nfs
  sources: 10.102.1.11/32 10.102.1.12/32
ssh
  sources: 10.102.1.1/32

# La zone par défaut est donc drop
[mattox@backup ~]$ sudo firewall-cmd --get-default-zone
drop

# Et on affiche la conf du firewall
[mattox@backup ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
(...)
  interfaces: enp0s3 enp0s8
(...)
[mattox@backup ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
(...)
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
(...)
[mattox@backup ~]$ sudo firewall-cmd --list-all --zone=nfs
nfs (active)
(...)
  sources: 10.102.1.11/32 10.102.1.12/32
  services: nfs
  ports: 19999/tcp
(...)
```

### D. Reverse Proxy

#### 🌞 Restreindre l'accès au reverse proxy `front.tp2.linux`

```bash
# On configure la zone drop par défaut
[mattox@front ~]$ sudo firewall-cmd --set-default-zone=drop
success

# On y ajoute l'interface
[mattox@front ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent

# On y ajoute une nouvelle zone ssh
[mattox@front ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success

# Seule l'IP 10.102.1.1 et le port 22 seront autorisés
[mattox@front ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[mattox@front ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success

# On crée une nouvelle zone proxy
[mattox@front ~]$ sudo firewall-cmd --new-zone=proxy --permanent
success

# Seules les machines du réseau 10.102.1.0/24 pourront joindre le proxy
[mattox@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24 --permanent
success
```

#### 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`

```bash
# On a donc 3 zones actives
[mattox@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32

# La zone par défaut est drop
[mattox@front ~]$ sudo firewall-cmd --get-default-zone
drop

# Et on liste la conf de notre firewall
[mattox@front ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
(...)
  interfaces: enp0s3 enp0s8
(...)
[mattox@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
(...)
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
(...)
[mattox@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
(...)
  sources: 10.102.1.0/24
(...)
```

### E. Tableau récap

#### 🌞 Rendez-moi le tableau suivant, correctement rempli :

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|----------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80          | `10.102.1.14`             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 3306        | `10.102.1.11`             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | 19999       | `10.102.1.11` & `10.102.1.12`             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | 443         | `10.102.1.1` à `10.102.1.254`             |
