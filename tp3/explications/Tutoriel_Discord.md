# Comment créer un bot 

## 1. Creer une application

#### Pour créer un bot, il faut tout d'abord vous rendre sur le [Developper Portal Discord](https://discord.com/developers/applications)

* Se rendre dans **Applications**

![](./images/application.png)

* Créer une application dans **New Application**

![](./images/new_application.png)


* Donner un nom à votre application

![](./images/application_name.png)

## 2. Activer votre Bot Discord 

* Se rendre dans **Bot**
  
![](./images/bot.png)

* Ajouter votre bot

![](./images/add_bot.png)

* Et vous pouvez retrouver votre Token (Identifiant de votre bot qui permet de le démarrer)

![](./images/token.png)

* Inserer votre token dans `/bot/TP3-Bot-Discord/config.json` en remplacant "BOT_TOKEN":"insert your token" -> "BOT_TOKEN":"token"

## 3. Inviter votre bot sur un serveur

* Se rendre dans **OAauth2**

![](./images/oauth2.png)

* Récuperer votre **Client ID**

* Se rendre sur [Discord Permissions Calculator](https://discordapi.com/permissions.html)

* Choisir **Administrator**

* Inserer votre Client ID

#### Vous avez créer un lien pour inviter le bot sur vos serveurs
