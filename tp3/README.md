# TP3 : Création d'un herbergement de bot Discord

## A. Sommaire

- [TP3 : Création d'un herbergement de bot Discord](#tp3--création-dun-herbergement-de-bot-discord)
  - [A. Sommaire](#a-sommaire)
  - [B. Setup](#b-setup)
    - [1. Pré-requis](#1-pré-requis)
    - [2. Server](#2-server)
  - [C. Installation](#c-installation)
    - [1. Installation du bot discord](#1-installation-du-bot-discord)
    - [2. Mise en place du bot discord](#2-mise-en-place-du-bot-discord)
    - [3. Installation de docker](#3-installation-de-docker)
  - [D. Docker](#d-docker)
    - [1. Service web](#1-service-web)
    - [2. Bot discord](#2-bot-discord)
  - [E. Conseils d'utilisations](#e-conseils-dutilisations)
    - [1. Update le bot discord](#1-update-le-bot-discord)
    - [2. Modifier le bot discord](#2-modifier-le-bot-discord)
    - [3. Modifier le service web](#3-modifier-le-service-web)
  - [F. Automatisation](#f-automatisation)
    - [Update.sh](#updatesh)
    - [Install.sh](#installsh)

## B. Setup

### 1. Pré-requis 

- Avoir une VM sous rocky-linux
    - Il est recommandé d'avoir une configuration suivante:
        - 2000 Mo de mémoire vive
        - 128 Mo de mémoire vidéo
        - Carte NAT
        - Carte Host-Only
    - On doit se connecter en ssh dessus

### 2. Server

- Récupérer les scripts dans le dossier [scripts](./scripts) grâce à la commande `scp` depuis votre hôte:
 
```bash
scp path/scripts user@<ip_adress>:/home/user/scripts
```

## C. Installation

### 1. Installation du bot discord

- On installe le module git

```bash
[mattox@bot_discord ~]$ sudo dnf install -y git
(...)
Complete!
```

- On installe la base du bot Discord

```bash
[mattox@bot_discord ~]$ sudo mkdir /bot/
[mattox@bot_discord ~]$ cd /bot/

# On clone le repository
[mattox@bot_discord bot]$ sudo git clone https://github.com/Mattox40400/TP3-Bot-Discord.git
Cloning into 'TP3-Bot-Discord'...
remote: Enumerating objects: 37, done.
remote: Counting objects: 100% (37/37), done.
remote: Compressing objects: 100% (25/25), done.
remote: Total 37 (delta 8), reused 32 (delta 6), pack-reused 0
Unpacking objects: 100% (37/37), 9.57 KiB | 700.00 KiB/s, done.ù

[mattox@bot_discord bot]$ cd TP3-Bot-Discord/

# On installe nodejs et les packages necessaires au bot
[mattox@bot_discord TP3-Bot-Discord]$ sudo dnf install -y nodejs
(...)
Installed:
  nodejs-1:10.24.0-1.module+el8.3.0+101+f84c7154.x86_64
  nodejs-full-i18n-1:10.24.0-1.module+el8.3.0+101+f84c7154.x86_64
  npm-1:6.14.11-1.10.24.0.1.module+el8.3.0+101+f84c7154.x86_64

Complete!
[mattox@bot_discord TP3-Bot-Discord]$ sudo npm init -y
Wrote to /bot/TP3-Bot-Discord/package.json:

{
  "name": "TP3-Bot-Discord",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "dependencies": {
    "discord.js": "^13.3.1"
  },
  "devDependencies": {},
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://github.com/Mattox40400/TP3-Bot-Discord.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "bugs": {
    "url": "https://github.com/Mattox40400/TP3-Bot-Discord/issues"
  },
  "homepage": "https://github.com/Mattox40400/TP3-Bot-Discord#readme"
}
[mattox@bot_discord TP3-Bot-Discord]$ sudo npm i discord.js
(...)
added 30 packages from 81 contributors and audited 30 packages in 6.251s

[mattox@bot_discord TP3-Bot-Discord]$ sudo npm i fs
(...)
added 1 package and audited 31 packages in 1.145s

[mattox@bot_discord TP3-Bot-Discord]$ sudo npm i node-fetch@2
(...)
added 1 package from 1 contributor and audited 32 packages in 1.068s
```

### 2. Mise en place du bot discord 

- Inserez votre token dans le fichier `/bot/TP3-Bot-Discord/config.json`, Suivez le [Tutoriel Discord](./explications/Tutoriel_Discord.md)

- Inviter le bot dans votre serveur Discord, qui est aussi expliqué dans le [Tutoriel Discord](./explications/Tutoriel_Discord.md)

### 3. Installation de docker

- On installe le module docker 

```bash
[mattox@bot_discord TP3-Bot-Discord]$ sudo dnf install -y docker
(...)
Complete!
```

- Ajouter le repo Docker-CE
  
```bash
[mattox@bot_discord TP3-Bot-Discord]$ sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo

[mattox@bot_discord TP3-Bot-Discord]$ sudo dnf install -y docker-ce docker-ce-cli containerd.io --allowerasing
(...)
Complete!
```

- Demarrer docker au demarrage du systeme
  
```bash
[mattox@bot_discord TP3-Bot-Discord]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.

[mattox@bot_discord TP3-Bot-Discord]$ sudo systemctl start docker
```

## D. Docker

### 1. Service web 

- On cree le dossier `webApp` où seront les dossiers du site web

```bash
[mattox@bot_discord TP3-Bot-Discord]$ sudo mkdir /webApp/
[mattox@bot_discord TP3-Bot-Discord]$ sudo touch /webApp/Dockerfile
```

- On instancie le lancement du service web avec docker

```bash
[mattox@bot_discord TP3-Bot-Discord]$ cd /webApp/

# On clone le repository
[mattox@bot_discord webApp]$ sudo git clone https://github.com/Mattox40400/TP3-Web.git
Cloning into 'TP3-Web'...
remote: Enumerating objects: 13, done.
remote: Counting objects: 100% (13/13), done.
remote: Compressing objects: 100% (9/9), done.
remote: Total 13 (delta 0), reused 10 (delta 0), pack-reused 0
Unpacking objects: 100% (13/13), 1.94 KiB | 662.00 KiB/s, done.

# On programme notre Dockerfile
[mattox@bot_discord webApp]$ sudo vim /webApp/Dockerfile
[mattox@bot_discord webApp]$ sudo cat /webApp/Dockerfile
FROM ubuntu
RUN apt update && apt upgrade -y
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
RUN apt-get install -y apache2
ENTRYPOINT /usr/sbin/apache2ctl -D FOREGROUND
RUN mv /var/www/html/index.html /var/www/html/index.html.old
RUN mkdir -p /site
COPY TP3-Web/ /site
RUN cd /site && cp * /var/www/html/
RUN service apache2 start

[mattox@bot_discord webApp]$ sudo docker build -t serviceweb .
(...)
Removing intermediate container 8e770004e9b1
 ---> c03b1479beab
Successfully built c03b1479beab
Successfully tagged serviceweb:latest
```

- On demarre le container du service Web sur le port 8080

```bash
[mattox@bot_discord TP3-Bot-Discord]$ sudo docker run -d -p 8080:80 serviceweb
d0e49f4adb1794170699ebc1a31fb3503542d4f22c6573aaa878dc282ba1c3f5
```

### 2. Bot discord

- On build l'image du bot discord

```bash
[mattox@bot_discord webApp]$ cd /bot/TP3-Bot-Discord
[mattox@bot_discord TP3-Bot-Discord]$ sudo docker build -t discordapp .
```

- On lance le container du bot

```bash
[mattox@bot_discord TP3-Bot-Discord]$ sudo docker run -d discordapp
2226cd0bb2269e827efa87737987f441ee55640bf19b90f1d23fe29b4b48ecb9
```

**Votre bot est maintenant en ligne ! `Essayer un $help !`**

## E. Conseils d'utilisations

### 1. Update le bot discord

A chaque modifications du site ou des fichiers du bot, il faut :

```bash
# Reperer les containers encore actifs
[mattox@bot_discord TP3-Bot-Discord]$ sudo docker ps
CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS         PORTS
    NAMES
051495cffd41   discordapp   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds
    inspiring_boyd
d0e49f4adb17   serviceweb   "/bin/sh -c '/usr/sb…"   4 minutes ago   Up 4 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   intelligent_turing

# On les termine pour lancer des nouveaux AVEC les modifications
[mattox@bot_discord TP3-Bot-Discord]$ sudo docker kill 05
05
[mattox@bot_discord TP3-Bot-Discord]$ sudo docker kill d0
d0

# On re-build les images
[mattox@bot_discord TP3-Bot-Discord]$ sudo docker build -t discordapp .
(...)
Successfully built a6a60d9cf10e
Successfully tagged discordapp:latest
[mattox@bot_discord TP3-Bot-Discord]$ cd /webApp
[mattox@bot_discord webApp]$ sudo docker build -t serviceweb .
(...)
Successfully built c03b1479beab
Successfully tagged serviceweb:latest

# Et on re-demarre les containers
[mattox@bot_discord webApp]$ sudo docker run -d discordapp
4c5de5b18e4a29dbd040a7eb901b4e11873d95f7432056c546deec3756f76628
[mattox@bot_discord webApp]$ sudo docker run -d -p 8080:80 serviceweb
c50ee6d1556476217f3e176ec3a00bbae328cadde1665d8ee2116683e139f7ad
```

Pour cela, le script `update.sh` va vous aider !
Ce script recherche les containers actifs et les termine, et relance automatiquement vos containers.

### 2. Modifier le bot discord

Le bot discord se trouve dans le dossier **/bot/TP3-Bot-Discord**

```bash
cd /bot/TP3-Bot-Discord
```

Vous pouvez modifier les fichiers qui sont à l'intérieur, mais veillez à garder `index.js` comme fichier principal

### 3. Modifier le service web

Le service web se trouve dans le dossier **/webApp/TP3-Web/**

```bash
cd /webApp/TP3-Web/
```

Vous pouvez modifier mettre les fichiers du site ici.
Le fichier HTML principal doit rester `index.html`


## F. Automatisation

Pour automatiser ces manipulations, les scripts suivant vont vous aider [`install.sh`](./scripts/update.sh) et [`update.sh`](./scripts/update.sh)

### Update.sh

Ce script comme expliqué plus haut, va automatiser vos modifications !
Lorsque vous voudrez relancer vos containers il suffit de faire : 

```bash
sudo sh update.sh
```

### Install.sh

Afin d'automatiser la mise en place du serveur, le script comprend:
  - Installation de git
  - Git clone du repository du bot discord
  - Installation de tous les packages necessaires au bot
  - Installation de docker
  - Demarage de docker
  - Instalation du Service Web
  - Lancement du Service Web
  - Installation du Service Docker du bot
  - Lancement de ce service

Pour le site web, si l'utilisateur en as déjà un, le bot deplacera juste le dossier dans /webApp/TP3-Web/, où le container utilise pour lancer le site web.

**Subtilité : L'installation se fait en 2 partie car vous devez inserer votre token entre les deux parties.**