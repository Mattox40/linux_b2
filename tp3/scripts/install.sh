#!/bin/bash
# Installation des ressources
# Mattéo FERREIRA SILVA - 07/11/2021

read -p "Quelle partie d'installation voulez vous lancer ? (1 ou 2)" partie
while [ "$partie" != "1" ] && [ "$partie" != "2" ];
do
    read -p "Réponse non correcte ! (1/2)" partie
done
if [ "$partie" = "1" ];
then
    dnf install -y git
    echo "Le dossier contenant le bot se trouvera donc dans /bot/TP3-Bot-Discord/"
    mkdir /bot/
    cd /bot/
    git clone https://github.com/Mattox40400/TP3-Bot-Discord.git
    cd TP3-Bot-Discord/
    dnf install -y nodejs
    npm init -y
    npm install discord.js
    npm install fs
    npm install node-fetch@2
    echo "Si vous n'avez pas créer de token, suivez le tutoriel \"Tutoriel_Discord.md\" dans le README"
    echo "Puis inserer votre token dans /bot/TP3-Bot-Discord/config.json (important)"
    echo "Apres avoir fait cela, vous pouvez relancer le script pour lancer la partie 2 d'installation"
else
    dnf install -y docker
    dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
    dnf install -y docker-ce docker-ce-cli containerd.io --allowerasing
    systemctl enable docker
    systemctl start docker

    mkdir /webApp/
    cd /webApp/
    touch Dockerfile
    echo "FROM ubuntu
    RUN apt update && apt upgrade -y
    RUN DEBIAN_FRONTEND=\"noninteractive\" apt-get -y install tzdata
    RUN apt-get install -y apache2
    ENTRYPOINT /usr/sbin/apache2ctl -D FOREGROUND
    RUN mv /var/www/html/index.html /var/www/html/index.html.old
    RUN mkdir -p /site
    COPY TP3-Web/ /site
    RUN cd /site && cp * /var/www/html/
    RUN service apache2 start" > /webApp/Dockerfile
    read -p "Avez-vous un site web pour votre bot discord ? (O/N)" rep
    while [ "$rep" != "O" ] && [ "$rep" != "N" ];
    do
        read -p "Réponse non correcte ! (O/N)" rep
    done
    if [ "$rep" = "O" ];
    then
        echo "\nAssurez vous que votre fichier principal se nomme 'index.html'"
        read -p "Où se situe ce dossier ?" path
        while [ ! -f "$path/index.html" ]; 
        do
            echo "\nIl n'y as pas de 'index.html' dans $path"
            read -p "Où se situe ce dossier ?" path
        done
        mkdir /webApp/TP3-Web
        cd $path
        cp * /webApp/TP3-Web/
    else
        cd /webApp/
        git clone https://github.com/Mattox40400/TP3-Web.git
    fi
    docker build -t serviceweb .
    docker run -d -p 8080:80 serviceweb

    cd /bot/TP3-Bot-Discord
    docker build -t discordapp .
    docker run -d discordapp
fi