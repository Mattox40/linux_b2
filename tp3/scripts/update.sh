#!/bin/bash
# Update site && bot
# Mattéo FERREIRA SILVA - 07/11/2021

if [ $(docker ps -q --filter ancestor=discordapp) ];
then
    docker kill $(docker ps -q --filter ancestor=discordapp)
fi
cd /bot/TP3-Bot-Discord/
docker build -t discordapp .
docker run -d discordapp
if [ $(docker ps -q --filter ancestor=serviceweb) ];
then
    docker kill $(docker ps -q --filter ancestor=serviceweb)
fi
cd /webApp/
docker build -t serviceweb .
docker run -d -p 8080:80 serviceweb